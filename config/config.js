/**
 * @config.js
 * This config page is used to declare server configurations
 *
 * @copyright 2019 melento.com
 */

var environment = "local";
var configurations = {};
configurations.loadConfigurations = loadConfigurations;
var configuration = {};


function loadConfigurations() {

    configuration.onPrimise = false;
    configuration.cipherKey = "Q5mo=2WMXKAwmTkbwBlftGd6LVZjVKXh";
    configuration.tempUploadPath = "./uploads/temp";
    configuration.environment = environment
    configuration.eSign_jar = "electronic_signature.jar"
    configuration.generateHash_jar = "getDocumentHash.jar";

   

    return configuration;
}
module.exports = configurations;