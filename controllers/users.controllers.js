var express = require('express');
var userService = require('../services/users.service');

var router =express.Router();
 router.post('/saveUsers',saveUser);
 router.post ('/fetchuser',fetchUsers);
 router.post ('/saveInformation',saveInformation);
 router.post('/fetchUserInformation',fetchUserInformation);
 router.post('/saveEmployee',saveEmployee);
 router.post('/saveStudent',saveStudent);

 module.exports=router;
console.log('user controllers');


 function saveUser(req,res,next){
    try {
        console.log("Inside SaveUser");
        userService.saveUsers(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the user data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the user data.' });
    }
}

function fetchUsers(req,res,next){
    // console.log("Inside Fetchuser Controllers")

    try {
    console.log("Inside Fetchuser Controllers")

        userService.fetchUsers()
            .then(function (result) {
                res.send(result);

            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the department data.' });
    }
}

function newsaveUsers(req,res,next){
    try {
        userService.savenewUsers(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.send({status:"failure",message:'Could not save user new data'});
        });
    }
    catch(err){
        res.send({status:"failure", message :'Could not save user new Data'});
    }
}

function saveInformation(req,res,next){
    try {
        userService.saveUserForm(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.send({status:"failure",message:'Could not save user new data'});
        });
    }
    catch(err){
        res.send({status:"failure", message :'Could not save user new Data'});
    }
}

function fetchUserInformation(req,res,next){
    try {
        console.log("Inside Fetchuser Controllers")
    
            userService.fetchUsersInfo(req.body)
                .then(function (result) {
                    res.send(result);
    
                })
                .catch(function (err) {
                    res.send({ status: "failure", message: 'Could not save the department data.' });
                });
    
        } catch (err) {
            res.send({ status: "failure", message: 'Could not save the department data.' });
        }
}

function saveEmployee(req,res,next){
    try {
        userService.saveEmployeeForm(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.send({status:"failure",message:'Could not save user new data'});
        });
    }
    catch(err){
        res.send({status:"failure", message :'Could not save user new Data'});
    }
}

function saveStudent(req,res,next){
    try {
        userService.saveStudentForm(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.send({status:"failure",message:'Could not save user new data'});
        });
    }
    catch(err){
        res.send({status:"failure", message :'Could not save user new Data'});
    }
}