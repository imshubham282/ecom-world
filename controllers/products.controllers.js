var express = require('express');
var productService = require('../services/products.service');

var router =express.Router();

router.post('/fetchproducts',fetchproducts);
router.post('/saveProducts',saveProducts);
// router .post('/fetchAllProducts',fetchAllProducts);
router.post('/fetchProductByType',fetchProductByType);


module.exports=router;
console.log('controllers')



function saveProducts(req,res,next){
    try {
        productService.saveProducts(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the department data.' });
    }
}

function fetchproducts(req,res,next){
    try {
        productService.getProducts()
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the department data.' });
    }
}


// function fetchAllProducts(req,res,next){
//     try {
//         productService.getProducts()
//             .then(function (result) {
//                 res.send(result);
//             })
//             .catch(function (err) {
//                 res.send({ status: "failure", message: 'Could not save the department data.' });
//             });

//     } catch (err) {
//         res.send({ status: "failure", message: 'Could not save the department data.' });
//     }
// }


function fetchProductByType(req,res,next){
    try {
        productService.getProductsbytype(req.body)
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the department data.' });
    }
}