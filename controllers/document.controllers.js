var express = require('express');
// var userService = require('../services/users.service');
var documentService =require ('../services/documents.service');

var router =express.Router();

router.post('/saveDocument',saveDocument);
router.post('/fetchDocuments',fetchDocuments);
router.post ('/fetchDocumentsData',fetchDocumentsData);
router.post ('/updateDocumentsData',updateDocumentsData);
router.post('/removeDocument',removeDocument);

console.log('product Controller');



module.exports=router;


function saveDocument(req,res,next){

    try {
        documentService.saveDocumentForm(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function(err){
            res.send({status:"failure",message:'Could not save user new data'});
        });
    }
    catch(err){
        res.send({status:"failure", message :'Could not save user new Data'});
    }
}

function fetchDocuments(req,res,next){
    try {
        documentService.getDocuments()
            .then(function (result) {
                res.send(result);
            })
            .catch(function (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            });

    } catch (err) {
        res.send({ status: "failure", message: 'Could not save the department data.' });
    }
}

function fetchDocumentsData(req,res,next){
    try{
        documentService.getDocumentsById(req.body)
        .then(function(result){
            res.send(result);
        })
        .catch(function (err){
            res.send({status:'failure',message:'Could not  fetch the document Data'});
        });
    } catch (err) {
                res.send({ status: "failure", message: 'Could not save the department data.' });
            }
    }


    function updateDocumentsData(req,res,next){
     
        try {
                    documentService.updateDocuments(req.body)
                        .then(function (result) {
                            res.send(result);
                        })
                        .catch(function (err) {
                            res.send({ status: "failure", message: 'Could not save the department data.' });
                        });
            
                } catch (err) {
                    res.send({ status: "failure", message: 'Could not save the department data.' });
                }
    }

    function removeDocument(req,res,next){
        try {
            documentService.removeDocuments(req.body)
                .then(function (result) {
                    res.send(result);
                })
                .catch(function (err) {
                    res.send({ status: "failure", message: 'Could not save the department data.' });
                });
    
        } catch (err) {
            res.send({ status: "failure", message: 'Could not save the department data.' });
        }
    }

// function fetchDocumentsData(req,res,next){
//     try {
//         documentService.getDocumentsById(req.body)
//             .then(function (result) {
//                 res.send(result);
//             })
//             .catch(function (err) {
//                 res.send({ status: "failure", message: 'Could not save the department data.' });
//             });

//     } catch (err) {
//         res.send({ status: "failure", message: 'Could not save the department data.' });
//     }

// }

// function updateDocumentsData(req,res,next){
//     try {
//         documentService.updateDocuments(req.body)
//             .then(function (result) {
//                 res.send(result);
//             })
//             .catch(function (err) {
//                 res.send({ status: "failure", message: 'Could not save the department data.' });
//             });

//     } catch (err) {
//         res.send({ status: "failure", message: 'Could not save the department data.' });
//     }

// }