var http = require('http');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var port =3000;


var products=require('./controllers/products.controllers');

var users =require('./controllers/users.controllers');
var document=require('./controllers/document.controllers');


var app = express();

app.use(function (req, res, next) { //allow cross origin requests

    res.setHeader("Access-Control-Allow-Methods", "POST,GET");
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
    var allowedOrigins =  "http://localhost:4200"
    var origin = req.headers.origin;
    origin = (origin != undefined) ? origin : req.headers.referer;
    if (allowedOrigins.indexOf(origin) > -1) {
        res.setHeader('Access-Control-Allow-Origin', origin);
    }
    //res.setHeader('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "Origin, x-parse-application-id,x-parse-rest-api-id,x-parse-rest-api-key, X-Requested-With,Authorization, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    //res.setHeader("Cache-Control", "max-age=2592000");
    next();
});
app.use(bodyParser.json({ limit: "20mb" }));
app.use(bodyParser.urlencoded({ limit: "20mb", extended: true, parameterLimit: 20000 }));
// httpServer.listen(config.port, function () {
//     if (config.environment != 'local' && __dirname.indexOf('var/www/html') != -1) {
//         var time = new Date(moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"));
//         // emailService.sendServerErrorNotification({ time: time, error: "Server Restarted", type: "Server Restarted" });
//     }
//     console.error('Server started on port', config.port, 'at:' + new Date())
// });
 app.use('/api/products',products);
 app.use('/api/users',users);
 app.use('/api/employee',users);
 app.use('/api/student',users);
 app.use('/api/document',document);
//  app.use('/api/usersform',users);
 
var httpServer = http.createServer(app);

httpServer.setTimeout(300000);

httpServer.listen(port, function () {
    
    console.error('Server started on port', port, 'at:' + new Date())
});