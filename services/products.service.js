var database = require('../config/database.js');
/**
 * Loading external libraries used
 */

var Q = require('q');
var _ = require('underscore');
var moment = require('moment');
var mongojs = require('mongojs');
const path = require('path');
var fs = require('fs');

var product = {};
product.getProducts = getProducts;
product.saveProducts=saveProducts;
product.getProductsbytype=getProductsbytype;
// product.getProducts=getProducts;
module.exports = product;

function getProducts() {
    var deferred = Q.defer();
    try {
        database.products.find( function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}

function saveProducts(params){
    console.log(params);

    var deferred = Q.defer();
    try {
        database.products.insert( {name:params.name,price:params.price,type:params.type,image:params.imagepath},function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}

function getProductsbytype(params){
    console.log("Params from Service>>>",params)
    console.log("database",database)

    var deferred = Q.defer();
    try {
        database.products.find( {type:params.type},function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}