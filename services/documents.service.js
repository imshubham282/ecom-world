var database = require('../config/database.js');


var Q = require('q');
var _ = require('underscore');
var moment = require('moment');
var mongojs = require('mongojs');
const path = require('path');
var fs = require('fs');

var document = {};

document.saveDocumentForm = saveDocumentForm;
document.getDocuments = getDocuments;
document.getDocumentsById = getDocumentsById;
// document.getDocumentsById=getDocumentsById;
// document.updateDocuments=updateDocuments;
document.updateDocuments=updateDocuments;
// document.deleteDocument=deleteDocument;
document.removeDocuments=removeDocuments;
module.exports = document;



function saveDocumentForm(params) {
    console.log("Params>>>>>>>", params)
    // console.log(params);
    // console.log("within saveProducts service.js")
    // console.log("database",database)
    var deferred = Q.defer();
    try {
        database.loans.insert({ doc_name: params.docName, doc_type: params.doctype, doc_number: params.docnumber,is_delete:false }, function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}
function getDocuments() {
    var deferred = Q.defer();
    console.log("database", database)

    // console.log("fetchUserInfo",params.email)
    try {
        database.loans.find(
            {is_delete:false},function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}

function getDocumentsById(data) {
    var deferred = Q.defer();
    try {
        database.loans.findOne({ _id: mongojs.ObjectID(data.id) },
            function (err, insertedDocument) {
                if (err) {
                    reportError.report({
                        where: 'getDocumentsById',
                        page: 'documents.service.js',
                        whereExctly: 'getDocumentsById() function',
                        errorTitle: 'Failed to fetch documents data',
                        errorInfo: err
                    });
                    deferred.reject(err);
                } else if (insertedDocument) {
                    console.log(insertedDocument)
                    deferred.resolve({ status: "success", message: "Fetched Successfully !", result: insertedDocument })
                }
            })
    }
    catch (exception) {
        console.log("Exception", exception)
    }
    return deferred.promise;
}

function updateDocuments(params){
    console.log("Params from doc service", params)

    var deferred=Q.defer();

    try{
        var updateParams={
            doc_name:params.docName,
            doc_type:params.doctype,
            doc_number:params.docnumber
        }
       
        database.loans.findAndModify({
            query:{
                _id:mongojs.ObjectID(params.docId),
            },
            update:{
                $set:updateParams
            },
            new:true
        },
        function(err,insertedDocument){
            if(err){
                reportError.report({
                    where:'updateDocuments',
                    page:'document.service.js',
                    whereExctly:'updateDocuments() function',
                    errorTitle:'Failed to update the document data',
                    errorInfo:err
                });
                deferred.reject(err);
            }
            else if(insertedDocument){
                console.log(insertedDocument);
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
                // deferred.resolve({result:"success",message:"Document Updated Successfully",result:insertedDocument})
           
            }
        })
    }
    catch(exception){
        console.error("Exception",exception);
    }
    return deferred.promise;
}



function removeDocuments(params){
    console.log("Params from doc service", params)

    var deferred=Q.defer();

    try{
       
        var updateParams={            
            is_delete:params.isDeleted
        }
        database.loans.findAndModify({
            query:{
                _id:mongojs.ObjectID(params.docId),
            },
            update:{
                $set:updateParams
            },
            new:true
        },
        function(err,insertedDocument){
            if(err){
                reportError.report({
                    where:'updateDocuments',
                    page:'document.service.js',
                    whereExctly:'updateDocuments() function',
                    errorTitle:'Failed to update the document data',
                    errorInfo:err
                });
                deferred.reject(err);
            }
            else if(insertedDocument){
                console.log(insertedDocument);
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
                // deferred.resolve({result:"success",message:"Document Updated Successfully",result:insertedDocument})
           
            }
        })
    }
    catch(exception){
        console.error("Exception",exception);
    }
    return deferred.promise;
}






// function deleteDocument(data){
//     console.log("Params>>>>>",data)
//     var deferred=Q.defer();
    
//     try {
//         database.documents.remove({ _id: mongojs.ObjectID(data.docId) },
//             function (err, insertedDocument) {
//                 if (err) {
//                     reportError.report({
//                         where: 'getDocumentsById',
//                         page: 'documents.service.js',
//                         whereExctly: 'getDocumentsById() function',
//                         errorTitle: 'Failed to fetch documents data',
//                         errorInfo: err
//                     });
//                     deferred.reject(err);
//                 } else if (insertedDocument) {
//                     console.log(insertedDocument)
//                     deferred.resolve({ status: "success", message: "Fetched Successfully !", result: insertedDocument })
//                 }
//             })
//     }
//     catch (exception) {
//         console.log("Exception", exception)
//     }
//     return deferred.promise;
// }
