/**
 * @genericService.service.js
 *
 * generic  functionalities are written below so that it can be reused
 *
 * @copyright 2019 melento.com
 */

/**
 * Loading Application level configuration data
 */
// var config = require('../config/config.js').loadConfigurations();
var database = require('../config/database.js');


/**
 * Loading in-house entity based organised modules to reuse the functionality
 */
var reportError = require('./reportError.service');
//var activityService = require('./activity.service');

/**
 * Loading external libraries used
 */
var Q = require('q');
var moment = require('moment');
var base64 = require('base-64');
var CryptoJS = require("crypto-js");
var crypto = require('crypto');


/**
 * List of all the functions available in this module
 */

var genericService = {};
genericService.encryptResponse = encryptResponse;
genericService.decryptRequest = decryptRequest;
genericService.getRandomId = getRandomId;
genericService.encryptMongoId = encryptMongoId;
genericService.genRandomString = genRandomString;
module.exports = genericService;


/* Following function is used for encrypting the API response 
 * before sending it to client
 * @encryptResponse(req, res, next)
 * 
 * 
 *
 */

function encryptResponse(params) {
    //if (req.body.encryprion_required) {
    //var encryptedResponse = CryptoJS.AES.encrypt(JSON.stringify(req.body.response_data), config.cipherKey).toString();
    var encryptedResponse = CryptoJS.AES.encrypt(JSON.stringify(params), config.cipherKey).toString();
    return encryptedResponse;

    //next()
    // } else {
    //next()
    //}

}


/* Following function is used for decrypt the API data from client 
 * 
 * @decryptRequest(req, res, next)
 * 
 * 
 *
 */
function decryptRequest(req, res, next) {
    var currentDate = new Date(moment().utc().format("YYYY-MM-DDTHH:mm:ssZ"));
    var subtractedDate = new Date(moment(moment(currentDate).subtract(60, 'minutes').toDate()).format("YYYY-MM-DDTHH:mm:ssZ"))
    var decryptedTokenId = req.body.token_id ? base64.decode(req.body.token_id) : '';
    var api_body = req.body.api_body

    database.sdi_login_tokens.findAndModify({
        query: { token_id: decryptedTokenId, is_used: false, created_at: { $gte: subtractedDate } },
        remove: true
        // update: {
        //     $set: {
        //         is_used: true,
        //     }
        // }
    }, function (err, token) {

        if (err) {
            console.error("error")
            next()
        }
        if (token) {
            var decryptedBody = CryptoJS.AES.decrypt(api_body.toString(), decryptedTokenId);
            var decryptedData = JSON.parse(decryptedBody.toString(CryptoJS.enc.Utf8));

            req.body.ip_address ? (decryptedData.ip_address = req.body.ip_address) : null
            req.body = decryptedData;
            req.body.encryption_required = true;
            next()

        } else {
            next()
        }
    });
}

/* Following function is used generate the random ID which will be used for encrypting API body
 * 
 * @getRandomId()
 * 
 * 
 *
 */
function getRandomId() {
    var deferred = Q.defer();
    try {
        var timeStamp = Math.floor(Date.now()) + genRandomString(2);

        var newToken = {
            token_id: timeStamp,
            created_at: new Date(moment().utc().format("YYYY-MM-DDTHH:mm:ssZ")),
            is_used: false
        }
        database.sdi_login_tokens.save(newToken, function (err, newToken) {
            if (err) {
                console.error("getRandomId err", err);
                reportError.report({
                    where: 'getRandomId ',
                    page: 'generic.service.js',
                    whereExctly: 'getRandomId() function',
                    errorTitle: 'Callback failed from save token query',
                    errorInfo: err,
                    input: newToken
                });
                deferred.reject(err);
            }
            else {
                var encryptToken = CryptoJS.AES.encrypt(timeStamp, config.cipherKey).toString();

                deferred.resolve(encryptToken);

            }
        })
    } catch (exception) {
        console.error("getRandomId exception", exception);
        reportError.report({
            where: 'getRandomId ',
            page: 'generic.service.js',
            whereExctly: 'getRandomId() function',
            errorTitle: 'exception ',
            errorInfo: exception,
        });
    }
    return deferred.promise;

}


/**
 *
 * Following function is used to generates random string of characters i.e salt
 *
 *
 * @genRandomString(length)
 *
 * @length {string}
 *  Length of the random string.
 *
 *
 */

function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')/** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
}

/**
 *
 * Following function is used to encrypt the mongo ID which will be used in the url, for sending it through email
 *
 *
 * @encryptMongoId(length)
 *
 * @mongoId {string}
 *  any mongo Id string.
 *
 *
 */
function encryptMongoId(mongoId) {
    var encryptedResponse = CryptoJS.AES.encrypt(JSON.stringify(mongoId), config.cipherKey).toString();
    return encryptedResponse;
}