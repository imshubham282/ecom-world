var database = require('../config/database.js');


var Q = require('q');
var _ = require('underscore');
var moment = require('moment');
var mongojs = require('mongojs');
const path = require('path');
var fs = require('fs');

var users={};
users.saveUsers=saveUsers;
users.fetchUsers = fetchUsers;
users.savenewUsers=savenewUsers;
users.saveUserForm=saveUserForm;
users.fetchUsersInfo=fetchUsersInfo;
users.saveEmployeeForm=saveEmployeeForm;
users.saveStudentForm=saveStudentForm;
module.exports=users;

function saveUsers(params){
    console.log("within saveUsers service.js")
    console.log("database",database)
    var deferred = Q.defer();
    try {
        database.users.insert( {name:params.newName,age:params.age,sex:params.gender,country:params.country,active:params.isActive},function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}



function fetchUsers() {
    console.log("within getPrdoucts service.js")
    console.log("database",database)
    var deferred = Q.defer();
    try {
        database.users.find( function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}

function savenewUsers(params) {
    console.log("within getPrdoucts service.js")
    console.log("database",database)
    var deferred = Q.defer();
    try {
        database.users.insert({name:params.name,age:params.age}, function (err, insertedDocument) {
            console.log("Params>>>>>>>>>>>>>>>"+ params)
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}


function saveUserForm(params){
    var deferred = Q.defer();
    try {
        database.users.insert({name:params.email,password:params.password}, function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}

function fetchUsersInfo(params){
    var deferred = Q.defer();
console.log("fetchUserInfo",params.email)
    try {
        database.users.findOne({name:params.email} ,function (err, insertedDocument) {
            if (err) {
                reportError.report({
                    where: 'getDepartmentById',
                    page: 'onboard.service.js',
                    whereExctly: 'getDepartmentById() function',
                    errorTitle: 'Failed to create fetch department data',
                    errorInfo: err
                });
                deferred.reject(err);
            } else if (insertedDocument) {
                console.log(insertedDocument)
                deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
            }
        })
    }
    catch (exception) {
        console.error("exception", exception);
    }
    return deferred.promise;
}
function saveEmployeeForm(paramsssssss){
    var deferred = Q.defer();
    console.log("saveEmployee",paramsssssss.empName)
        try {
            database.employee.insert({name:paramsssssss.empName,password:paramsssssss.empPassword} ,function (err, insertedDocument) {
                if (err) {
                    reportError.report({
                        where: 'getDepartmentById',
                        page: 'onboard.service.js',
                        whereExctly: 'getDepartmentById() function',
                        errorTitle: 'Failed to create fetch department data',
                        errorInfo: err
                    });
                    deferred.reject(err);
                } else if (insertedDocument) {
                    console.log(insertedDocument)
                    deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
                }
            })
        }
        catch (exception) {
            console.error("exception", exception);
        }
        return deferred.promise;
}

function saveStudentForm(params){
    var deferred = Q.defer();
        try {
            database.student.insert({name:params.studenName,password:params.studentPassword} ,function (err, insertedDocument) {
                if (err) {
                    reportError.report({
                        where: 'getDepartmentById',
                        page: 'onboard.service.js',
                        whereExctly: 'getDepartmentById() function',
                        errorTitle: 'Failed to create fetch department data',
                        errorInfo: err
                    });
                    deferred.reject(err);
                } else if (insertedDocument) {
                    console.log(insertedDocument)
                    deferred.resolve({ status: "success", message: "Fetched successfully!", result: insertedDocument });
                }
            })
        }
        catch (exception) {
            console.error("exception", exception);
        }
        return deferred.promise;
}