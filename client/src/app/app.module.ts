//built-in
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {ProductsModule} from './products/products.module';
import {ProductService} from './services/product.service';
import  {UserService} from './services/user.service';
import {DocumentService} from './services/document.service'

import {InitialProductsModule} from './initial-products/initial-products.module';
import {MainProductsModule} from './main-products/main-products.module'

//components

import { ReactiveFormsModule }    from '@angular/forms';
import { appRoutes } from './routes';
import {FullLayoutComponent} from './layout/full-layout.component';

// import { ProductsComponent } from './products/products.component';


@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule ,
    ProductsModule,
    InitialProductsModule,
    MainProductsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ProductService,UserService,DocumentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
