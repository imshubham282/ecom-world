import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { ProductService } from '../services/product.service';


@Component({
  selector: 'app-main-products',
  templateUrl: './main-products.component.html',
  styleUrls: ['./main-products.component.css']
})
export class MainProductsComponent implements OnInit {
  @Output('displayProducts')
  displayProducts = new EventEmitter<any>();
 


  image_name:any;
  show:any;
  imagePath:any;

  productArray:any=[];
  showProductofType:any=[];


  constructor(private productservice:ProductService) { }

  ngOnInit() {
    this.fetchProducts();

  }

  addProduct(formvalue){
    var params={
      name:formvalue.name,
      price:formvalue.price,
      type:formvalue.type,
      imagepath:this.imagePath
    }
   console.log("Form Value", formvalue)
    this.productservice.saveProducts(params).subscribe(data=>{
      this.productArray=data.result;

      console.log("productArray",this.productArray)

    })

  }
  fetchProducts(){
    // this.productservice.fetchProducts().subscribe(data=>{
      // this.productArray=data.result;

      var params={
        type:'men'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType)        
          })

      // console.log("productArray",this.productArray)

    // })
  }
  active_link(tabnumber,tabname){
    if(tabnumber==1 && tabname=='man'){
      var params={
        type:'men'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType)        
          })

    }
    else if(tabnumber==2 && tabname=='woman'){
      console.log(tabname,tabnumber)
      var params={
        type:'women'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType.result)        
          })
    }
    else if(tabnumber==3 && tabname=='kids'){
      console.log(tabname,tabnumber)
      var params={
        type:'kids'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType",this.showProductofType.result)        
          })
    }
    else if(tabnumber==4 && tabname=='accessories'){
      console.log(tabname,tabnumber)
      var params={
        type:'accessories'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        // console.log(data);
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType.result)
        
          })
    }

  }

  

}
