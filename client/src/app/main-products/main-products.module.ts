import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainProductsRoutingModule} from './main-products-routing.module';
import {MainProductsComponent} from './main-products.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    MainProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
    ],
  declarations: [
    MainProductsComponent
  ],
  exports: [ MainProductsRoutingModule,CommonModule,MainProductsComponent],
  providers: [],
 


})
export class MainProductsModule { 
    
}
