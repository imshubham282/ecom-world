import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainProductsComponent } from './main-products.component';


const routes: Routes = [
    {
        path: '',
        data: {
          title: 'initial-products'
        },
        children: [
          {
            path: '',
            component: MainProductsComponent,
           
            data: {
              title: ' '
            }
        
          }
        ]
    }
          
          
  ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainProductsRoutingModule { }
