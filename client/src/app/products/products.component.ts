import { Component, OnInit } from '@angular/core';
import {ProductService} from '../services/product.service';
import {UserService} from '../services/user.service';
import {DocumentService} from '../services/document.service'
import {FormBuilder,FormGroup,FormControl} from '@angular/forms'
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  username:any;
  password:any;
  producName:any;
  productPrice:any;
  productSize:any;
  showProducts:[];
  showDocuments:[];
  productId:any;

  userForm:FormGroup;
  employeeForm:FormGroup;
  studentForm:FormGroup;
  documentForm:FormGroup;

  constructor(private formbuilder:FormBuilder,  private prodcutservice:ProductService,private userservice:UserService,private documentservice:DocumentService) { 
    this.userForm=this.formbuilder.group({
      'username':[null],
      'password':[null]
    })

    this.employeeForm=this.formbuilder.group({
       'empname':[null],
       'emppassword':[null]
    })
    this.studentForm=this.formbuilder.group({
      'studentname':[null],
      'studentpassword':[null]
    })
    this.documentForm=this.formbuilder.group({
      'docName':[null],
      'doctype':[null],
      'docnumber':[null]

    })
  }

  ngOnInit() {  
    console.log("Products Components")
      // this.getProductData();
      // this.getUserData();
  }
  // getProductData(){
  //   this.prodcutservice.fetchProduct().subscribe(data=>{
      
  //   });
  // }
  saveData(){
   var params={
     name:'aaa',
     age:'22'     
   }
   console.log(params);
   this.prodcutservice.saveProducts(params).subscribe(data=>{

   });
  //  make a call to product.service.ts - 
 }
 newSaveData(){
     var newData={
       newName:'Shubham',
       age:25,
       gender:'Male',
       country:'India',
       isActive:true
     }
     console.log(newData);
     this.userservice.saveUserName(newData).subscribe(newdatas=>{

     })
     
}
getUserData(){
  this.userservice.fetchUsers().subscribe(userData=>{

  });
}


saveUser(){
  var userdata={
    name:"Tania",
    age:"22"
  }
  this.userservice.saveUser(userdata).subscribe(usernewdata=>{

  })
}


saveForm(userDetail){
  var params={
    email:userDetail.username,
    password:userDetail.password
  }
  console.log('User Detail', userDetail.username)

  this.userservice.saveUserForm(params).subscribe(userinfo=>{

  })
}

fetchUserInfo(){
  var params={
    email:"demo.test.admin@legaldesk.com"
  }
  this.userservice.fetchUserData(params).subscribe(userInfo=>{
console.log(userInfo);
this.username=userInfo.result.name;
this.password=userInfo.result.password;
  })
}

saveEmpForm(empDetail){
  var params={
    empName:empDetail.empname,
    empPassword:empDetail.emppassword
  }
  console.log("params>>>>>>>>", params)
  this.userservice.saveEmployeeForm(params).subscribe(empInfo=>{

  })
}

saveStudentForm(studentDetail){
var params={
  studenName:studentDetail.studentname,
  studentPassword:studentDetail.studentpassword
}
console.log("Student name",params.studenName )
this.userservice.saveStudentForm(params).subscribe(studentInfo=>{

})
}
fetchStudent(){
  this.userservice.fetchstudentData().subscribe(studentData=>{
    console.log(studentData);
    // for(var i=0;i<studentData.result.length;i++){
    //   this.showProducts=studentData.result[i]
    // }
      this.showProducts=studentData.result;
      console.log(this.showProducts);

    // this.producName=studentData.result[0].product_name;
    // this.productPrice=studentData.result[0].product_price;
    // this.productSize=studentData.result[0].product_size;
  });
}

saveDocumentForm(params){
  var docDetail={
    documentName:params.docName,
    documentType:params.doctype,
    documentNumber:params.docnumber
  }
  this.documentservice.saveDocument(params).subscribe(data=>{

  })
console.log(docDetail);
this.fetchDocument();

}

fetchDocument(){
  
  this.documentservice.fetchDocument().subscribe(data=>{
    // console.log(data.result[3].doc_name);
    this.showDocuments=data.result;

  });
}

editDetail(data){
  console.log(data)
this.productId=data._id;
var params={
  id:data._id
  
}

this.documentservice.getDataById(params).subscribe(data=>{
if(data.status=='success'){
  this.documentForm.controls['docName'].setValue(data.result.doc_name);
  this.documentForm.controls['doctype'].setValue(data.result.doc_type);
  this.documentForm.controls['docnumber'].setValue(data.result.doc_number);

}
})

}

updateDocument(data){
 
  var params={
    docId:this.productId,
    docName:data.docName,
    doctype:data.doctype,
    docnumber:data.docnumber
  }
  this.documentservice.updateDocData(params).subscribe(data=>{
    console.log("Data",data)
    if(data.status=='success'){
      this.fetchDocument();
    }
  })
}


deleteDoc(data){
  
  console.log("data",data)
  var params={
    isDeleted:true,
    docId:data._id
  }
  // var params={
  //   docId:data._id
  // }
  // this.documentservice.deleteDocdata(params).subscribe(data=>{

  // })
  this.documentservice.deleteDocdata(params).subscribe(data=>{
    console.log("Data",data)
    if(data.status=='success'){
      this.fetchDocument();
    }
  })
  console.log("delete",params);
  this.fetchDocument();

}

}
