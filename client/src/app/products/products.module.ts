import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProductsRoutingModule} from './products-routing.module';
import {ProductsComponent} from './products.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'

@NgModule({
  imports: [
    CommonModule,
    ProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule
    ],
  declarations: [
    ProductsComponent
  ],
  exports: [ ProductsRoutingModule,CommonModule,ProductsComponent],
  providers: [],
 


})
export class ProductsModule { }
