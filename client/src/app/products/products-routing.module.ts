import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsComponent } from './products.component';


const routes: Routes = [
    {
        path: '',
        data: {
          title: 'products'
        },
        children: [
          {
            path: '',
            component: ProductsComponent,
           
            data: {
              title: ' '
            }
        
          }
        ]
    }
          
          
  ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
