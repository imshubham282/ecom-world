import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitialProductsComponent } from './initial-products.component';


const routes: Routes = [
    {
        path: '',
        data: {
          title: 'initial-products'
        },
        children: [
          {
            path: '',
            component: InitialProductsComponent,
           
            data: {
              title: ' '
            }
        
          }
        ]
    }
          
          
  ];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitialProductsRoutingModule { }
