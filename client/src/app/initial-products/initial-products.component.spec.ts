import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialProductsComponent } from './initial-products.component';

describe('InitialProductsComponent', () => {
  let component: InitialProductsComponent;
  let fixture: ComponentFixture<InitialProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
