import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import {InitialProductsRoutingModule} from './initial-products-routing.module';
import {InitialProductsComponent} from './initial-products.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms'
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  imports: [
    CommonModule,
    InitialProductsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
    ],
  declarations: [
    InitialProductsComponent
  ],
  exports: [ InitialProductsRoutingModule,CommonModule,InitialProductsComponent],
  providers: [],
 


})
export class InitialProductsModule { 
    
}
