import { Component, OnInit } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import {FormBuilder,FormGroup,FormControl} from '@angular/forms'
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-initial-products',
  templateUrl: './initial-products.component.html',
  styleUrls: ['./initial-products.component.css']
})
export class InitialProductsComponent implements OnInit {
  public LogoUploader: FileUploader = new FileUploader({});

  AddProducts:FormGroup;

  uploadFileError:boolean=false;
  uploadFileErrorMessage:any;
  image_name:any;
  show:any;
  imagePath:any;

  productArray:any=[];
  showProductofType:any=[];

  constructor(private formbuilder:FormBuilder,private productservice:ProductService) {

    this.AddProducts=this.formbuilder.group({
      "name":[null],
      "price":[null],
      "type":[null],
      
      
    })
    this.LogoUploader.onAfterAddingAll = (image) => {
      console.log(image)
      if (image[0].file.size >= 5000000) {
        this.uploadFileError = true;
        if (this.uploadFileError == true) {
          this.uploadFileErrorMessage = "Your image file cannot exceed 5 MB";
        }
        image[0].remove();
      }
      else {
        this.uploadFileError = false;
        this.uploadFileErrorMessage = '';
        this.image_name = image[0].file.name
      }
      var fileCount: number = this.LogoUploader.queue.length;
      if (fileCount > 0) {
        this.show = true;
        this.LogoUploader.queue.forEach((val, i, array) => {
          var fileReader = new FileReader();
          fileReader.onloadend = (e) => {
            var imageData = fileReader.result;
            this.imagePath = imageData;
            console.log(this.imagePath)
            // var image = <HTMLImageElement>document.getElementById("client_logo");
            // image.src = this.imagePath;
          }
          fileReader.readAsDataURL(val._file);
        })
      }
    };
   }

  ngOnInit() {
    this.fetchProducts();
  }

  addProduct(formvalue){
    var params={
      name:formvalue.name,
      price:formvalue.price,
      type:formvalue.type,
      imagepath:this.imagePath
    }
   console.log("Form Value", formvalue)
    this.productservice.saveProducts(params).subscribe(data=>{
      this.productArray=data.result;

      console.log("productArray",this.productArray)

    })

  }
  fetchProducts(){
    // this.productservice.fetchProducts().subscribe(data=>{
      // this.productArray=data.result;

      var params={
        type:'men'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType)        
          })

      // console.log("productArray",this.productArray)

    // })
  }
  active_link(tabnumber,tabname){
    if(tabnumber==1 && tabname=='man'){
      var params={
        type:'men'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType)        
          })

    }
    else if(tabnumber==2 && tabname=='woman'){
      console.log(tabname,tabnumber)
      var params={
        type:'women'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType.result)        
          })
    }
    else if(tabnumber==3 && tabname=='kids'){
      console.log(tabname,tabnumber)
      var params={
        type:'kids'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        this.showProductofType=data.result;
        console.log("showProductofType",this.showProductofType.result)        
          })
    }
    else if(tabnumber==4 && tabname=='accessories'){
      console.log(tabname,tabnumber)
      var params={
        type:'accessories'
      }
      this.productservice.fetchProductData(params).subscribe(data=>{
        // console.log(data);
        this.showProductofType=data.result;
        console.log("showProductofType", this.showProductofType.result)
        
          })
    }

  }
}
