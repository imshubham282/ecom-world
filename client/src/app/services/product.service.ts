import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import * as _ from 'underscore';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { AppConfig } from '../app.config';
import { find } from 'rxjs/operators';

@Injectable()
export class ProductService {

 constructor(private http: HttpClient) { }


 
//  fetchProduct() {
//      console.log("fetchProduct")
//  var headers = new HttpHeaders();

//  headers.append('Content-Type', 'application/json');
//  return this.http.post('http://localhost:3000/api/products/fetchproducts',  { headers: headers })
//  .map((data: any) => {
//  return data;
//  });
//  }

saveProducts(params){
    var headers = new HttpHeaders();
     headers.append('Content-Type', 'application/json');
     return this.http.post('http://localhost:3000/api/products/saveProducts',params, { headers: headers })
     .map((data: any) => {
     return data;
     });
    }


fetchProducts(){
 var headers = new HttpHeaders();
 headers.append('Content-Type', 'application/json');
 return this.http.post('http://localhost:3000/api/products/fetchproducts',  { headers: headers })
 .map((data: any) => {
 return data;
 });
 }

fetchProductData(params){
    var headers = new HttpHeaders();

 headers.append('Content-Type', 'application/json');
 return this.http.post('http://localhost:3000/api/products/fetchProductByType',params, { headers: headers })
 .map((data: any) => {
 return data;
 });
}



 
};