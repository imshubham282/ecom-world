import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import * as _ from 'underscore';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { AppConfig } from '../app.config';
import { find } from 'rxjs/operators';


@Injectable()
export class DocumentService {
 
  constructor(private http: HttpClient) {

  }
  saveDocument(params){
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/document/saveDocument',params,{headers:headers})
    .map((data:any)=>{
        return data;
    })
  }
  fetchDocument(){
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/document/fetchDocuments',{headers:headers})
    .map((data:any)=>{
        return data;
    })
  }
  getDataById(params){

    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/document/fetchDocumentsData',params,{headers:headers})
    .map((data:any)=>{
      return data;
    })
  }

  updateDocData(params){
      var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/document/updateDocumentsData',params,{headers:headers})
    .map((data:any)=>{
        return data;
    }) 
  }
 
  deleteDocdata(params){
    console.log("Service Data>>>" , params)
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/document/removeDocument',params,{headers:headers})
    .map((data:any)=>{
      return data;
    })

  }
}