import { Injectable } from '@angular/core';
import { Observable, Subject, of } from 'rxjs';
import * as _ from 'underscore';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch';
import { AppConfig } from '../app.config';
import { find } from 'rxjs/operators';

@Injectable()
export class UserService {

 constructor(private http: HttpClient) { }

//  saveUserName(newData){
//     console.log("SaveProducts");
//     console.log("Params from Service" + newData);
//     var headers = new HttpHeaders();
  
//    headers.append('Content-Type', 'application/json');
//    return this.http.post('http://localhost:3000/api/products/saveProducts',newData, { headers: headers })
//    .map((data: any) => {
//    return data;
//    });
//    }
saveUserName(newData){
    console.log("Params from Service" + newData);
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/users/saveUsers',newData,{headers:headers})
    .map((newdatas:any)=>{
        return newdatas;
    });
}
fetchUsers(){
    console.log("Inside Fetch User Service")
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/users/fetchuser',{headers:headers})
    .map((userData:any)=>{
        return userData;
    })
   }
   saveUser(userdata){
       var headers=new HttpHeaders();
       headers.append('Content-Type','application/json');
       return this.http.post('http://localhost:3000/api/users/newsaveUsers',userdata,{headers:headers})
       .map((usernewdata:any)=>{
           return usernewdata;
       })
   }
   fetchNewUsers(){
    var headers = new HttpHeaders();

    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/products/getNewProducts', { headers: headers })
    .map((data: any) => {
    return data;
    });
 }
 saveUserForm(params){
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/users/saveInformation',params,{headers:headers})
    .map((usernewdata:any)=>{
        return usernewdata;
    })
 }

 fetchUserData(params){
    var headers=new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/users/fetchUserInformation', params,{ headers: headers })
    .map((data: any) => {
    return data;
    }); 
 }

 saveEmployeeForm(params){
    var headers=new HttpHeaders();
    headers.append('Content-Type','application/json');
    return this.http.post('http://localhost:3000/api/employee/saveEmployee',params,{headers:headers})
 }

 saveStudentForm(params){
     var headers=new HttpHeaders();
     headers.append('Content-Type','application/json');
     return this.http.post('http://localhost:3000/api/student/saveStudent',params,{headers:headers})
     .map((studentInfo:any)=>{
         return studentInfo;
     })
 }

 fetchstudentData(){
    var headers = new HttpHeaders();

    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/products/fetchAllProducts', { headers: headers })
    .map((data: any) => {
    return data;
    });
 }
};