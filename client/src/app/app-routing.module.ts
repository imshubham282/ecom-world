import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FullLayoutComponent } from './layout/full-layout.component';


const routes: Routes = [
//   {
//   path: '',
//   data:{
//     title:'Product'
//   },
//   children:[
//     {
//       path:'prodcuts',
//       loadChildren:'./products/products.module#ProductsModule'
//     }
//   ]
// }

{
  path: '',
  redirectTo: 'initial-products',
  pathMatch: 'full',
  //  canActivate : [AuthGuard]
},

{
  path: '',            //<---- parent component declared here
  component: FullLayoutComponent,
  // resolve: {
  //   userInfo: AppResolver
  // },
  data: {
    title: 'Document'
  },
  children: [
    {
      path: 'initial-products',
      loadChildren: './initial-products/initial-products.module#InitialProductsModule',
      // canActivate: [AuthGuard]
    },
  
  
   

  ]
}

  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
